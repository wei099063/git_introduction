# Git教學及Git Lab平台教學

## 	一、版本控制系統

![本地檔案版本](img/001.png)

版本控制系統是一種軟體工程的開發技巧，可以透過這個系統讓每位成員的軟體版本可以方便同步和維護管理（不然要用 email 或是其他工具傳送和管理十分麻煩，尤其程式又常常會有不同版本修改的問題！）。在沒有版本控制系統時，我們常會在編輯檔案前複製一個備份，或是在更新檔案後產生許多重複檔案(如上圖)，非常不便且難以維護。因此，使用版本控制系統的需求應運而生。

一般在軟體開發中又分為中央式系統[^1] 與分散式系統（本次要使用的Git），分散式系統讓不同開發者直接在各自的本地檔案庫工作，並容許多個開發者同時更動同一個檔案，而每個檔案庫有另外一個合併各個改變的功能。分散式系統讓開發者能在沒有網路的情況下也能繼續工作，也讓開發者有充分的版本控制能力。

## 二、Git基本觀念

![Git流程圖](img/002.png)

Git 可以分為 Local（本地）和 Remote（遠端）兩個環境，由於 Git 屬於分散式的版本控制系統，所以開發者可以在離線 local 環境下開發，等到有網路時再將自己的程式推到 Remote 環境或 pull 下其他開發者程式碼進行整合。在 Local 中我們又分為 working directory（工作資料夾）、staging area（暫存區）和 repositories（檔案庫）。

當自己開發時會在工作資料夾工作，當要進入檔案庫之前會先將檔案加入暫存區，確認沒問題則 commit 到檔案庫中，最後 push 上去 remote 環境。在 Git 中若是有和其他開發者一起合作，則會需要處理不同 branch 之間 conflict 和 merge 的問題。

下一個段落會介紹Gitlab平台的使用方式，有興趣的同學可以參考一下[GitLab & GitHub差異](https://about.gitlab.com/devops-tools/github-vs-gitlab/)。

## 三、GitLab教學

### Windows系統：

首先先安裝[Git for Windwos](https://gitforwindows.org/)，基本上按照預設選項一路安裝即可，安裝完成後重開機。

### Linux ：

如果你想要透過二進位安裝程式安裝基本的 Git 工具集，你通常可以直接透過你所用的發行版（distribution）內建的基礎套件管理工具。 舉例來說，如果你使用的是 Fedora，你可以使用 yum：

```console
$ sudo yum install git-all
```

如果你是使用 Debian 系列的發行版，如 Ubuntu，你可以使用 apt-get：

```console
$ sudo apt-get install git-all
```

如果需要更多選擇，Git 官方網站上有更多其他的發行版中安裝 Git 的安裝步驟，網址為 http://git-scm.com/download/linux。

###  OSX：

#### 1. 從官方網站下載 Mac 專屬版本的 Git

網站：https://git-scm.com/download/mac

只要下載檔案、點開、執行，基本上這樣就可順利完成了。

#### 2. 使用 Homebrew 軟體來安裝 Git

![Homebrew](img/003png)

雖然 Mac 作業系統出廠的時候已經有很多軟體或工具了，但對開發者來說很多工具還是得自己想辦法下載甚至得從原始程式碼進行編譯、安裝。而 Homebrew 這個工具就是用來補足這個缺口，它有點像在 Linux 的 `apt-get` 之類的安裝工具，通常只要一行指令就可完成下載、編譯、安裝。如果您本身也是開發者，建議可考慮使用 Homebrew 來安裝軟體。

網址：https://brew.sh/index_zh-tw.html

安裝方式很簡單，就是只要照著它網站上的那行複製起來：

```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

在終端機視窗貼上並執行就可以了。

Homebrew 安裝完成後，請一樣在終端機下執行這行指令：

```
$ brew install git
```

意思就是請 Homebrew 幫你安裝 Git 這個軟體，按下 Enter 鍵之後，剩下的就交給 Homebrew 去煩惱了。

### 申請GitLab帳號：

網站：https://gitlab.com/users/sign_up

#### Step1：填入帳號密碼信箱

![image-20210802180953733](img/004.png)

#### Step2：去信箱收驗證信

![image-20210802181338256](img/005.png)

#### Step3：重新登入GitLab並填寫基本資料

![image-20210802181626102](img/006.png)

若申請帳號為個人使用記得選擇Just me。

![image-20210802181916421](img/007.png)

這邊要設定第一個專案的group基本上按照自己的意願填寫就好了

![image-20210802182102269](img/008.png)

第一個專案名稱(這邊其實只是範例)基本上你的專案網址就會是

```
以Group創立：https://gitlab.com/GroupName/projectName/
以User創立：https://gitlab.com/UserName/projectName/
```

到這邊就申請完帳號了。進到`DashBoard`就可以看到我們剛剛以群組身分建立的專案了。

### 建立新專案

![image-20210802182800901](img/009.png)

點選` New project`

![image-20210802182950637](img/010.png)

點選 `Create blank project`

![image-20210802183057486](img/011.png)

`Project name `: 專案名稱

`URL` : 基本上會照上述提到的邏輯自動生成，若不滿意也可以使用自訂的URL但第二個網域一定是自己的UserID或GroupName

`Project description` : 專案描述，會先幫你寫在在`README`的第一行，可以不寫。

Visibility Level : 

​	`Private` : 不會公布在網路上，沒有存取權的人不能看到該專案，可以設定某用戶的訪問權限(稍後介紹)

​	`Public` : 專案可以在網路上被訪問到，無論有沒有存取權都能看到這個專案且有fork .pull request的權限。

​	PS : 無論是哪一種權限 沒有設定`developer`或`mainner`都不能進行commit & push 的動作。

![image-20210802194621235](img/012.png)

1. 專案的名稱

2. 
   1. `Commit` : 提交的紀錄，可視為每一條commit就是一個版本。
   2. `Branch` : 分支 詳見 [連結](http://gitlab.9skin.com/wei/git-flow)
   3. `Tag` : 如果有發布版本通常會在這邊新增版本的簡介(或很單純的是版本號而已)

3. `Fork` : 把別人的專案叉到自己的專案底下(形象大概就是把他的盤子裏面的肉叉到到自己的盤子裏面)，原本的專案擁有者可以看到Fork紀錄(如果有commit  也看的到) ，主要是為了研究或是幫忙開發功能，Fork別人的人開發完之後可以向專案擁有者提出pull request 如果專案擁有者同意，就可以把別人開發的功能加到自己的專案裡面。簡單來說就是網路上的陌生人之間的協同開發。
4. `Clone` : 從Web上面把專案 "克隆" 下來，有分為SSH及HTTPS兩種下載方式，SSH的優點是安全性高、速度快，缺點則是操作複雜及成本較高(server需要有SSH憑證)，HTTPS是直接透過GitLab的帳號密碼就可以將程式碼複製到自己的電腦裡優點是方便、操作簡單，缺點則是安全性較低(較嚴格的協作標準通常要掛VPN到有GitLab網域[^2]擁有權的電腦申請SSH連線)、速度較慢。
5. 目前專案內的檔案
   1. `Name` : 檔案名稱
   2. `last commit` : 該檔案最後修改是在哪一條commit



### 將新專案下載到電腦中

* 以下以[該專案](https://gitlab.com/wei099063/git_introduction)示範

​	首先先將使用者名稱存到git的全域變數內(告訴電腦裡面的git工具你是誰才能抓私有權限的專案)

```
git config --global user.name "yourusername"
git config --global user.email "youruseremail"
```



![image-20210802201007220](img/013.png)

複製`Clone with HTTPS`的連結

```
指令 : git clone 連結

範例 : 

$git clone https://gitlab.com/wei099063/git_introduction 
Cloning into 'git_introduction'...
warning: redirecting to https://gitlab.com/wei099063/git_introduction.git/
remote: Enumerating objects: 51, done.
remote: Counting objects: 100% (51/51), done.
remote: Compressing objects: 100% (41/41), done.
Receiving objects:  56% (29/51)used 41 (delta 8), pack-reused 0 eceiving objects:  54% (28/51)
Receiving objects: 100% (51/51), 716.23 KiB | 2.27 MiB/s, done.
Resolving deltas: 100% (10/10), done.

假設CMD位置在C:/ Clone下來的專案會在 C:/專案名稱/ 裡面
```

![image-20210802203248905](img/014.png)



### 常用指令說明 : 

在專案中新增了一個檔案 `test.txt`

![image-20210802203431288](img/015.png)

可以先用

```
git status
```

查看本地目錄上面有變動的檔案

```
C:\Users\yie09\OneDrive\文件\Code\git_introduction>git status
On branch main
Your branch is up to date with 'origin/main'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        test.txt

nothing added to commit but untracked files present (use "git add" to track)
```

可以看到Untracked的檔案有剛剛新增`test.txt`，Untracked 的意思是，Git 偵測到你有新增這筆檔案，但尚未是 Git 追蹤的對象。所以必須先將它加入到 staging area(索引)裡，就可以將 `test.txt` 加入到追蹤對象。

那麼要如何將檔案加入到索引呢？指令是：

```undefined
git add <檔案名稱>
```

以本案例來說，就必須用`git add test.txt` 來加入索引。如果要一口氣將全部檔案加入索引，可以用 `git add .`

加入索引後，可以再次輸入 `git status` 觀看，會得到

```
C:\Users\yie09\OneDrive\文件\Code\git_introduction>git status
On branch main
Your branch is up to date with 'origin/main'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   test.txt
```

會發現該檔案狀態本來是 `Untracked files`，變成是`Changes to be committed`，這樣就表示有成功加入到索引。

`Changes to be committed`的意思是，放在索引的檔案即將會被提交成一個新版本(commit)。

此時我們可以透過以下指令，來提交一個新版本。

```undefined
git commit -m "<填寫版本資訊>"
```

所以這裡我就輸入`git commit -m "新增文件:test.txt"`來提交版本後，系統如果回饋以下訊息，就成功透過 Git 建立出第一個版本出來了。

```
C:\Users\yie09\OneDrive\文件\Code\git_introduction>git commit -m "新增文件:test.txt"
[main df2cedc] 新增文件:test.txt
 2 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test.txt
```

可以再次使用 `git status` 觀察，會獲得以下資訊：

```undefined
On branch master
nothing to commit, working tree clean
```

因為你已經將剛剛的`test.txt`提交成一個版本(commit)，所以目前工作目錄上已經清空。

那麼我們要如何觀看新增的版本呢？你可以使用以下指令：

```undefined
git log
```

假使你有看到一個版本更新紀錄，表示你成功了！

```undefined
commit b6c3c771cd8939bcd25a8c50089fdf0cd3eab98d (HEAD -> master)
Author: 姓名 <Email>
Date:   版本更新時間

    新增文件:test.txt
```

若要退出，鍵入小寫 `q`。

![](C:\Users\yie09\OneDrive\文件\Code\git_introduction\img\016.png)

整體的邏輯就如上圖所示

### git pull 下載同步更新

`git push` 是在推送資料到遠端數據庫，那麼到底該把遠端數據庫更新的檔案抓下來呢？此時就會使用到 `git pull` 來下載同步更新。

通常會使用到 `git pull`，比較常見的情境，就是這個遠端數據庫有多人同步開發的關係才有可能。

## 情境介紹

早上上班時，A 開發者將自己的本地數據庫，push 到 GitHub 上進行同步更新。

![](img/017.png)

中午時， A 開發者告訴 B 說：「我放到 GitHub 上了，你再 clone 下來吧！」。於是 B 就照做了，於是三方目前的狀態都是一樣的。

![images](img/018.png)

下班前，A 又更新了一個 commit，於是又透過 push 推送到 GitHub 上，並告訴 B 說：「我有丟新版本上去哦，你要記得抓下來更新嘿。」

![images](img/019.png)

此時的重頭戲來了，這時候的 B 就必須下此指令。

```undefined
git pull
```

透過這指令，B 就能下載遠端的 master 分支，更新本地端的 master 資料，就會變成下圖。

![images](img/020.png)

所以實務開發時，常會透過 `git pull` 指令來抓取遠端數據庫資料來做更新。以避免本地端跟遠端資料落差太大。







待補充





[^1]: 如：[Subversion](https://zh.wikipedia.org/wiki/Subversion)、[CVS ](https://zh.wikipedia.org/wiki/協作版本系統)中央式版本控制系統的工作主要在一個伺服器進行，由中央管理存取權限「鎖上」檔案庫中的檔案，一次只能讓一個開發者進行工作。
[^ 2 ]: 企業可以向GitLab公司申請屬於自己的網域(管理者擁有更多權限，安全性也更高)

# 參考資料

https://blog.techbridge.cc/2018/01/17/learning-programming-and-coding-with-python-git-and-github-tutorial/

https://w3c.hexschool.com/git/3a1a8767

https://www.maxlist.xyz/2018/11/02/git_tutorial/





